<?php
// print_r('checking NEW');
// echo PHP_EOL;

$q = "SELECT * FROM users";
$users = mysqli_query($link, $q);

$curl = curl_init();
$headers = array(
    'Content-Type: application/json',
    'X-Auth-Token:xAVpheE7v6RFLHShixjAfjTlTG+jvTUk9ZvR65mWxow='
);

$d = time()-$timeDifference;
$date = $d*1000;

$url = 'https://kaspi.kz/shop/api/v2/orders?page[number]=0&page[size]=1000&filter[orders][state]=NEW&filter[orders][creationDate][$ge]='.$date;
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

$out = curl_exec($curl);
curl_close($curl);
$res = json_decode($out);
$orders = $res->data;

foreach ($orders as &$order) {
    $q = "SELECT * FROM orders WHERE order_id = ".$order->attributes->code;
    $result = mysqli_query($link, $q);
    $row = mysqli_fetch_array($result);

    // $filter = ['order_id' => $order->attributes->code];
    // $options = [];
    // $query = new MongoDB\Driver\Query($filter, $options);
    // $cursor = $manager->executeQuery('KaspiOrdersNTF.orders', $query);
    // foreach ($cursor as $document) {
        // $cursorArray = $cursor->toArray();
        if(!isset($row)){
            print_r('THERE IS NEW ORDER');
            echo PHP_EOL;

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $order->relationships->entries->links->self);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            $out = curl_exec($curl);
            $products = json_decode($out)->data;
            curl_close($curl);

            // foreach($array as $product){
            //     print_r("product id: ".$product->id."\n");
            //     $product_id = $product->id;

            //     curl_setopt($curl, CURLOPT_URL, 'https://kaspi.kz/shop/api/v2/orders/'.$order->id.'/entries');
            //     $array = json_decode(curl_exec($curl))->data;
            //     $quantity = '';
            //     $price = '';
            //     foreach($array as $product){
            //         $quantity .= $array[0]->attributes->quantity.";";
            //         $price .= $array[0]->attributes->totalPrice.";";
            //     }
                
            //     curl_setopt($curl, CURLOPT_URL, 'https://kaspi.kz/shop/api/v2/orderentries/'.$product_id.'/product');
            //     $name = json_decode(curl_exec($curl))->data->attributes->name;
            // }

            $product_id = $products[0]->id;
            $curl = curl_init();
            $url = 'https://kaspi.kz/shop/api/v2/orders/'.$order->id.'/entries';
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            $out = curl_exec($curl);
            curl_close($curl);
            $res = json_decode($out);
            $array = $res->data;
            $quantity = $array[0]->attributes->quantity;
            $price = $array[0]->attributes->totalPrice;

            $curl = curl_init();
            $url = 'https://kaspi.kz/shop/api/v2/orderentries/'.$product_id.'/product';
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            $out = curl_exec($curl);
            curl_close($curl);
            $res = json_decode($out);
            $name = $res->data->attributes->name;

            if(!isset($order->attributes->deliveryAddress)){
                print_r($order->attributes->code);
                $delieveryAddress='нет';
                $town = 'нет';
                echo PHP_EOL;
            }else{
                $delieveryAddress=$order->attributes->deliveryAddress->formattedAddress;
                $town = $order->attributes->deliveryAddress->town;
            }

            $order_id = $order->attributes->code;
            $state = $order->attributes->state;
            $status = $order->attributes->status;
            $cust_fname = $order->attributes->customer->firstName;
            $cust_lname = $order->attributes->customer->lastName;
            $cust_phone = $order->attributes->customer->cellPhone;
            $address = $delieveryAddress;
            $quantity = $quantity;
            $createdDate = date("Y-m-d H:i:s",$order->attributes->creationDate/1000);
            $url = 'https://kaspi.kz/merchantcabinet/#/orders/details/'.$order->attributes->code;
            $total_price = intval($order->attributes->totalPrice)+intval($order->attributes->deliveryCost);
            $product_name = $name;
            // $delivery_cost = intval($order->attributes->deliverySubsidyCost);
            $delivery_cost = intval($order->attributes->deliveryCostForSeller);

            $town = $town;
            $step = 'new';

            //save to mysql db
            $q = "INSERT INTO orders (`order_id`, `product_name`, `total_price`, `cust_fname`, `cust_lname`, `address`, `cust_phone`, `state`, `status`, `createdDate`, `step`, `town`, `delivery_cost`, `url`, `quantity`)
                VALUES ('$order_id', '$product_name', '$total_price', '$cust_fname', '$cust_lname', '$address', '$cust_phone', '$state', '$status', '$createdDate', '$step', '$town', '$delivery_cost', '$url', '$quantity')";
            $result = mysqli_query($link, $q);
            print_r($order_id.' inserted new order.');

            //send to tg
            foreach ($users as $user) {
                try{
                    // print_r($user['chat_id']);
                    $reply = "🟢 <b>НОВЫЙ ЗАКАЗ</b>".
                    "\n<b>Товар: </b> <i>$product_name</i>".
                    "\n<b>ID заказа: </b> <i>$order_id</i>".
                    "\n<b>Имя: </b> <i>$cust_fname $cust_lname</i>".
                    "\n<b>Адрес: </b> <i>$address</i>".
                    "\n<b>Телефон: </b> <i> +7$cust_phone</i>".
                    "\n<b>Стоимость: </b> <i>$total_price тг.</i>".
                    "\n<b>Количество: </b> <i>$quantity</i>".
                    "\n<b>Ссылка: </b> <i> $url</i>";
                    $telegram->sendMessage([ 'chat_id' => $user['chat_id'], 'text' => $reply ,'parse_mode'=>'HTML']);
                }catch(Exception $e) {
                    //delet user if he has blocked the bot
                    if($e->getMessage()==='Forbidden: bot was blocked by the user'){
                        print('delete this user: '.$user['chat_id']);
                        //update in mysql db
                        $chat_id = $user['chat_id'];

                        $q = "DELETE FROM users WHERE chat_id = '$chat_id'";
                        $result = mysqli_query($link, $q);
                    }
                }
            }


            // $newOrder = [
            //     'order_id' => $order->attributes->code,
            //     'state' => $order->attributes->state,
            //     'status' => $order->attributes->status,
            //     'cust_fname' => $order->attributes->customer->firstName,
            //     'cust_lname' => $order->attributes->customer->lastName,
            //     'cust_phone' => $order->attributes->customer->cellPhone,
            //     'address' => $delieveryAddress,
            //     'quantity' => $quantity,
            //     'createdDate' => new MongoDB\BSON\UTCDateTime($order->attributes->creationDate),
            //     'url' => 'https://kaspi.kz/merchantcabinet/#/orders/details/'.$order->attributes->code,
            //     'total_price' => intval($order->attributes->totalPrice),
            //     'product_name' => $name,
            //     'delivery_cost' => intval($order->attributes->deliveryCost),
            //     'town' => $town,
            //     'step' => 'new',
            //     'createdAt' => new MongoDB\BSON\UTCDateTime(),
            //     'updatedAt' => new MongoDB\BSON\UTCDateTime(),

            // ];
            // print_r($newOrder);
            // echo PHP_EOL;

            //save to db 
            // $bulk = new \MongoDB\Driver\BulkWrite;
            // $_id1 = $bulk->insert($newOrder);
            // $result = $manager->executeBulkWrite('KaspiOrdersNTF.orders', $bulk);
        }
        // relationships.entries.links.self
    // }
}

    
?>
