<?php
// require __DIR__ . '/vendor/autoload.php';
// include('../vendor/autoload.php'); 
$client = new \Google_Client();
$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
$client->setAccessType('offline');
$client->setAuthConfig(__DIR__.'/credentials.json');
$service = new Google_Service_Sheets($client);
$spreadsheetId = "1_agep7clDCft8vgi_of-G2buupMfR9V--oOdfodQWQY"; 
$params = ['valueInputOption' => 'USER_ENTERED'];
$get_range = "Движение денег!A:Z";

function addRow($order_id, $start_date, $name, $town, $price , $dostavka, $corier, $quantity){
    global $service;
    global $spreadsheetId;
    global $params;
    global $get_range;

    $response = $service->spreadsheets_values->get($spreadsheetId, $get_range);
    $values = $response->getValues();
    $index = count($values)+1;

    $sebes = "=ЕСЛИ(ЕПУСТО(E".$index.")=ЛОЖЬ();UNIQUE(FILTER('Себестоимость товаров'!\$H$2:\$H;'Себестоимость товаров'!\$C$2:\$C=E".$index."));\"\")";
    $bank = "=(M".$index."-I".$index.")*10/100";
    $one_percent = "=M".$index."/100*1";
    $expected_income = "=M".$index."-L".$index."";
    // $total_sebes = "=СУММ(F".$index.":K".$index.")";
    $total_sebes = "=СУММ(F".$index."*K".$index.";G".$index.";H".$index.";I".$index.";J".$index.")";
    
    $values = [['',$order_id,$start_date,'',$name,$sebes,$bank,$one_percent,$dostavka,$corier,$quantity,$total_sebes,$price,'',$expected_income,'','',$town]];
    $body = new Google_Service_Sheets_ValueRange(['values' => $values]);
    $upd_range = 'Движение денег!A'.$index.':Z'.$index;
    $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);


}

function updateDostavka($order_id,$dostavka,$cenaProdazhi){
    global $service;
    global $spreadsheetId;
    global $params;
    global $get_range;

    $response = $service->spreadsheets_values->get($spreadsheetId, $get_range);
    $values = $response->getValues();

    foreach($values as $key=>$value) {
        // echo "\n".$value[1];
        if($value[1]===strval($order_id)){
            $index = $key+1;  
                
            break;
        }
    }

    if(isset($index)){
        echo $index."\n";
        $body = new Google_Service_Sheets_ValueRange(['values' => [[$dostavka]]]);
        $upd_range = 'Движение денег!I'.$index.':J'.$index;
        $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);

        $body = new Google_Service_Sheets_ValueRange(['values' => [[$cenaProdazhi]]]);
        $upd_range = 'Движение денег!M'.$index.':N'.$index;
        $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);
    }
  
}
function updateRowComp($order_id){
    global $service;
    global $spreadsheetId;
    global $params;
    global $get_range;

    $response = $service->spreadsheets_values->get($spreadsheetId, $get_range);
    $values = $response->getValues();

    foreach($values as $key=>$value) {
        if($value[1]===$order_id){
            $index = $key+1;
            break;
        }
    }

    if(isset($index)){
        $income = "=M".$index."-L".$index."";

        $body = new Google_Service_Sheets_ValueRange(['values' => [[$income,'']]]);
        $upd_range = 'Движение денег!N'.$index.':O'.$index;
        $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);
        
        $body = new Google_Service_Sheets_ValueRange(['values' => [[date("d.m.Y")]]]);
        $upd_range = 'Движение денег!D'.$index.':E'.$index;
        $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);
    
        $body = new Google_Service_Sheets_ValueRange(['values' => [['Да']]]);
        $upd_range = 'Движение денег!P'.$index.':Q'.$index;
        $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);    
    }
}

function updateRowCancel($order_id){
    global $service;
    global $spreadsheetId;
    global $params;
    global $get_range;

    
    $response = $service->spreadsheets_values->get($spreadsheetId, $get_range);
    $values = $response->getValues();

    foreach($values as $key=>$value) {
        if($value[1]===$order_id){
            $index = $key+1;
            break;
        }
    }

    if(isset($index)){
        $body = new Google_Service_Sheets_ValueRange(['values' => [['ОТМЕНЕН']]]);
        $upd_range = 'Движение денег!A'.$index.':B'.$index;
        $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);
    
        $values = [['0','0','0','0','0','0','0','0','0'.'0','0','Нет']];
        $body = new Google_Service_Sheets_ValueRange(['values' => $values]);
        $upd_range = 'Движение денег!F'.$index.':P'.$index;
        $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);
    }

}

function updateRowWaitReturn($order_id){
    global $service;
    global $spreadsheetId;
    global $params;
    global $get_range;

    $response = $service->spreadsheets_values->get($spreadsheetId, $get_range);
    $values = $response->getValues();

    foreach($values as $key=>$value) {
        if($value[1]===$order_id){
            $index = $key+1;
            break;
        }
    }

    if(isset($index)){
        $body = new Google_Service_Sheets_ValueRange(['values' => [['ОЖИДАЕТ ВОЗВРАТА']]]);
        $upd_range = 'Движение денег!A'.$index.':B'.$index;
        $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);
    
        $values = [['0','0','0','0','0','0','0','0','0'.'0','0','Нет']];
        $body = new Google_Service_Sheets_ValueRange(['values' => $values]);
        $upd_range = 'Движение денег!F'.$index.':P'.$index;
        $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);
    }
}

function updateRowReturn($order_id){
    global $service;
    global $spreadsheetId;
    global $params;
    global $get_range;   

    $response = $service->spreadsheets_values->get($spreadsheetId, $get_range);
    $values = $response->getValues();

    foreach($values as $key=>$value) {
        if($value[1]===$order_id){
            $index = $key+1;
            break;
        }
    }
    
    if(isset($index)){
        $body = new Google_Service_Sheets_ValueRange(['values' => [['ВОЗВРАТ']]]);
        $upd_range = 'Движение денег!D'.$index.':E'.$index;
        $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);
    
        $values = [['0','0','0','0','0','0','0','0','0'.'0','0','Нет']];
        $body = new Google_Service_Sheets_ValueRange(['values' => $values]);
        $upd_range = 'Движение денег!F'.$index.':P'.$index;
        $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);    
    }
}

// updateRowCancel('146645700');

// $order_id = '146345300';
// $start_date = '15.02.2021';
// $name = 'DSP KM1003 белый';
// $town = 'Павлодар';
// $price = 24000;
// $dostavka = '';
// $corier = '';
// $quantity = 2;
// updateRowReturn($order_id)
// addRow($order_id, $start_date, $name, $town, $price, $dostavka, $corier,$quantity);

function updateRowReturned($order_id){
    global $service;
    global $spreadsheetId;
    global $params;
    global $get_range;

    $response = $service->spreadsheets_values->get($spreadsheetId, $get_range);
    $values = $response->getValues();

    foreach($values as $key=>$value) {
        if($value[1]===$order_id){
            $index = $key+1;
            break;
        }
    }
    
    if(isset($index)){
        $body = new Google_Service_Sheets_ValueRange(['values' =>  [['ОТМЕНЕН']]]);
        $upd_range = 'Движение денег!A'.$index.':B'.$index;
        $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);
    
        $body = new Google_Service_Sheets_ValueRange(['values' => [['ВЕРНУЛСЯ']]]);
        $upd_range = 'Движение денег!D'.$index.':E'.$index;
        $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);
    
        $values = [['0','0','0','0','0','0','0','0','0'.'0','0','Нет']];
        $body = new Google_Service_Sheets_ValueRange(['values' => $values]);
        $upd_range = 'Движение денег!F'.$index.':P'.$index;
        $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);    
    }
}

function getOrderIds(){
    global $service;
    global $spreadsheetId;
    global $params;
    global $get_range;   

    $response = $service->spreadsheets_values->get($spreadsheetId, $get_range);
    $values = $response->getValues();

    foreach($values as $key=>$value) {
        echo $value[1].",";
    }
    
    // if(isset($index)){
    //     $body = new Google_Service_Sheets_ValueRange(['values' => [['ВОЗВРАТ']]]);
    //     $upd_range = 'Движение денег!D'.$index.':E'.$index;
    //     $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);
    
    //     $values = [['0','0','0','0','0','0','0','0','0'.'0','0','Нет']];
    //     $body = new Google_Service_Sheets_ValueRange(['values' => $values]);
    //     $upd_range = 'Движение денег!F'.$index.':P'.$index;
    //     $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);    
    // }
}

?>
